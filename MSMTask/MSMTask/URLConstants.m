//
//  URLConstants.m
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import "URLConstants.h"

#define BaseUrl @"http://localhost:8080/simplefruit/api"

@implementation URLConstants

+ (NSURL *)getFruitsUrl
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/fruit", BaseUrl]];
}

+ (NSURL *)getFruitImageUrlForImageId:(NSString *)imageId
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/image/%@", BaseUrl, imageId]];
}

@end
