//
//  FruitTableViewCell.m
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import "FruitTableViewCell.h"
#import "FruitModel.h"

@interface FruitTableViewCell ()
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;
@property (nonatomic, strong) IBOutlet UIImageView *mainImageView;
@end

@implementation FruitTableViewCell

- (void)configureForFruitModel:(FruitModel *)fruitModel
{
    self.nameLabel.text = fruitModel.name;
    self.priceLabel.text = fruitModel.price;
}

- (void)setImageViewWithImage:(UIImage *)image
{
    [self.mainImageView setImage:image];
    
    [self.activityIndicator stopAnimating];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.nameLabel.text = nil;
    self.priceLabel.text = nil;
    self.mainImageView.image = nil;
}

@end