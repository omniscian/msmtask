//
//  GetFruitResponseModel.m
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import "GetFruitResponseModel.h"
#import "FruitModel.h"

@implementation GetFruitResponseModel

- (instancetype)initWithData:(NSData *)data
{
    if ((self = [super init]))
    {
        [self parseData:data];
    }
    
    return self;
}

- (void)parseData:(NSData *)data
{
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    if (!error)
    {
        NSMutableArray *mutableFruitsArray = [NSMutableArray array];
        
        for (NSDictionary *fruitDictionary in [dataDictionary objectForKey:@"fruit"])
        {
            FruitModel *fruitModel = [[FruitModel alloc] initWithDictionary:fruitDictionary];
            
            [mutableFruitsArray addObject:fruitModel];
        }
        
        self.fruitsArray = mutableFruitsArray;
    }
}

@end