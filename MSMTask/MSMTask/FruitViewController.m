//
//  ViewController.m
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import "FruitViewController.h"
#import "GetFruitResponseModel.h"
#import "FruitTableViewCell.h"
#import "FruitModel.h"
#import "URLConstants.h"

@interface FruitViewController ()
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation FruitViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self requestFruit];
}

- (void)requestFruit
{
    [self performAsyncRequestWithUrl:[URLConstants getFruitsUrl] withCompletionBlock:^(BOOL success, NSData *data) {
        if (success && data)
        {
            GetFruitResponseModel *responseModel = [[GetFruitResponseModel alloc] initWithData:data];
            self.dataSource = responseModel.fruitsArray;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Request failed" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *tryAgainAction = [UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self requestFruit];
                }];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:tryAgainAction];
                [alertController addAction:cancelAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    }];
}

- (void)performAsyncRequestWithUrl:(NSURL *)url withCompletionBlock:(void (^)(BOOL success, NSData *data))completionBlock
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data.length > 0)
        {
            completionBlock(YES, data);
        }
        else
        {
            completionBlock(NO, nil);
        }
        
    }];
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FruitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"FruitCell"];
    
    FruitModel *fruit = self.dataSource[indexPath.row];
    
    [cell configureForFruitModel:fruit];
    
    if (!fruit.fruitImage)
    {
        [cell.activityIndicator startAnimating];
        
        [self performAsyncRequestWithUrl:[URLConstants getFruitImageUrlForImageId:fruit.fruitId] withCompletionBlock:^(BOOL success, NSData *data) {
            if (success && data)
            {
                UIImage *image = [[UIImage alloc] initWithData:data];
                fruit.fruitImage = image;
                
                [cell setImageViewWithImage:fruit.fruitImage];
            }
        }];
    }
    else
    {
        [cell setImageViewWithImage:fruit.fruitImage];
    }
    
    return cell;
}

@end
