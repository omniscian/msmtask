//
//  ViewController.h
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FruitViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

