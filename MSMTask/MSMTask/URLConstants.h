//
//  URLConstants.h
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLConstants : NSObject

+ (NSURL *)getFruitsUrl;
+ (NSURL *)getFruitImageUrlForImageId:(NSString *)imageId;

@end
