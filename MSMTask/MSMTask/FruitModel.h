//
//  FruitModel.h
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FruitModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *fruitId;
@property (nonatomic, strong) UIImage *fruitImage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
