//
//  FruitModel.m
//  MSMTask
//
//  Created by Ian Layland-Houghton on 24/02/2016.
//  Copyright © 2016 Ian Layland-Houghton. All rights reserved.
//

#import "FruitModel.h"

@implementation FruitModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        [self setupModelForDictionary:dictionary];
    }
    
    return self;
}

- (void)setupModelForDictionary:(NSDictionary *)dictionary
{
    self.name = [dictionary objectForKey:@"name"];
    self.price = [dictionary objectForKey:@"price"];
    self.fruitId = [dictionary objectForKey:@"id"];
}

@end
